# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
movies = Movie.create([{title: 'Kill Bill: Vol. 1', year: '2003', rating: 7}, {title: 'Kill Bill: Vol. 2', year: '2004', rating: 8}, {title: 'Pulp Fiction', year: '1994', rating: 9}]);
actors = Actor.create([{first_name: 'Uma', last_name: 'Thurman', date_of_birth: '1970-04-29'}, {first_name: 'Lucy', last_name: 'Liu', date_of_birth: '1968-12-02'}, {first_name: 'John', last_name: 'Travolta', date_of_birth: '1954-02-18'}])
roles = Role.create([{actor: actors.first, movie: movies.first, name: 'The Bride'}, {actor: actors.first, movie: movies.second, name: 'Beatrix Kiddo'}, {actor: actors.first, movie: movies.third, name: 'Mia Wallace'}, {actor: actors.second, movie: movies.first, name: 'O-Ren Ishii'}, {actor: actors.second, movie: movies.second, name: 'O-Ren Ishii'}, {actor: actors.third, movie: movies.third, name: 'Vincent Vega'}])