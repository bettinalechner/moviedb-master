class RolesController < ApplicationController
	def new
		@role = Role.new
		if params[:actor_id]
			@actor = Actor.find(params[:actor_id])
			render 'new_role'
		elsif params[:movie_id]
			@movie = Movie.find(params[:movie_id])
			render 'new_cast'
		else
			raise 'Actor or Movie must be defined'
		end
	end

	def create
		if params[:actor_id]
			@actor = Actor.find(params[:actor_id])
			if @actor.roles.create(role_params)
				redirect_to @actor
			else
				render 'new'
			end
		elsif params[:movie_id]
			@movie = Movie.find(params[:movie_id])
			if @movie.roles.create(role_params)
				redirect_to @movie
			else
				render 'new'
			end
		else
			raise 'Actor and Movie must be defined'
		end
	end

	def edit
		@role = Role.find(params[:id])
	end

	def update
		@role = Role.find(params[:id])
		if @role.update(role_params)
			redirect_to @role.actor
		else
			render 'edit'
		end
	end

	def destroy
		@role = Role.find(params[:id])
		@actor = @role.actor
		@role.destroy

		redirect_to @actor
	end

	private

	def role_params
		params.require(:role).permit(:movie_id, :actor_id, :name)
	end
end
