class Actor < ActiveRecord::Base
	has_many :roles
	has_many :movies, through: :roles

	validates :first_name, presence: true
	validates :last_name, presence: true
	validates :date_of_birth, presence: true

	default_scope { order(:last_name, :first_name) }

	def name
		[self.first_name, self.last_name].join(' ')
	end

	def age
		Date.today.year - self.date_of_birth.year - ((Date.today.month > self.date_of_birth.month || (Date.today.month == self.date_of_birth.month && Date.today.day >= self.date_of_birth.day)) ? 0 : 1)
	end
end
