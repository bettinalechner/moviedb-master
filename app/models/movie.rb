class Movie < ActiveRecord::Base
	has_many :roles
	has_many :actors, through: :roles

	validates :title, presence: true
	validates :year, presence: true, numericality: { only_integer: true, greater_than: 1800, less_than: Date.today.year + 2.years }
	validates :rating, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 10, allow_blank: true }

	default_scope { order(:title) }
end
